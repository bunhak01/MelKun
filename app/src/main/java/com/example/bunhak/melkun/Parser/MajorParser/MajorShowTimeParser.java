package com.example.bunhak.melkun.Parser.MajorParser;

import android.os.AsyncTask;
import android.util.Log;

import com.example.bunhak.melkun.Class.Movie;
import com.example.bunhak.melkun.Interface.CallbackShowTime;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bunha on 8/12/2017.
 */

public class MajorShowTimeParser extends AsyncTask<Void, Void, List<Movie>> {

    private List<Movie> movieList;
    private CallbackShowTime callbackShowTime;

    public MajorShowTimeParser(CallbackShowTime callbackShowTime) {
        movieList = new ArrayList<>();
        this.callbackShowTime = callbackShowTime;
    }

    @Override
    protected void onPreExecute() {
        callbackShowTime.onPreExecute();
    }

    @Override
    protected List<Movie> doInBackground(Void... voids) {
        String baseUrl = "http://www.majorcineplex.com.kh";
        String url = baseUrl + "/cinema/showtimes";
        Document document = null;
        int j=0;
        try {
            document = Jsoup.connect(url).get();
            Elements movies = document.select("ul#detail_containTab li.detailContent1 div.contentTabs div.row-movie-list");
            for (Element moviess : movies) {
                Elements moviesss=moviess.select("div.showtimes-row-theatre");
                for(Element movie :moviesss ) {
                    j=0;
                    String posterUrl = movie.select("div.row-movie-list-img img").first().attr("src");
                    String title = movie.select("div.row-movie-list-detail.showtimes-row-movie-list-detail h2").text();
                    String link = movie.select("div.row-movie-list-img a").first().attr("href");
                    Movie item = new Movie(title, posterUrl, link, "Major");
                    for(Movie i:movieList){
                        if(i.getTitle().equals(title)){
                            j=1;
                            break;
                        }
                    }
                    if(j==0) {
                        movieList.add(item);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            callbackShowTime.onError();
        }
        return movieList;
    }

    @Override
    protected void onPostExecute(List<Movie> movieList) {
        callbackShowTime.onPostExecute(movieList);
    }
}

