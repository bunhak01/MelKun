package com.example.bunhak.melkun.Parser.PrimeParser;

import android.os.AsyncTask;
import android.util.Log;

import com.example.bunhak.melkun.Class.Movie;
import com.example.bunhak.melkun.Interface.CallbackComingSoon;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bunha on 8/12/2017.
 */

public class PrimeComingSoonParser extends AsyncTask<Void, Void, List<Movie>> {

    private List<Movie> movieList;
    private CallbackComingSoon callbackComingSoon;

    public PrimeComingSoonParser(CallbackComingSoon callbackComingSoon) {
        movieList = new ArrayList<>();
        this.callbackComingSoon = callbackComingSoon;
    }

    @Override
    protected void onPreExecute() {
        callbackComingSoon.onPreExecute();
    }

    @Override
    protected List<Movie> doInBackground(Void... voids) {
        String baseUrl = "http://202.62.37.232";
        String url = baseUrl + "/Browsing/Movies/ComingSoon";
        Document document = null;
        try {
            document = Jsoup.connect(url).get();
            Elements movies = document.select("article#movies-list div.list-item.movie");
            Log.e("ooooo", String.valueOf(movies.size()) );
            for (Element movie : movies) {
                String posterUrl = "http:"+movie.select("div.image-outer img").first().attr("src");
                String title = movie.select("div.item-details h3.item-title").first().text();
                String link = "http:"+ movie.select("div.item-details div.title-wrapper a").first().attr("href");
                String showdate=movie.select("div.item-details p.movie-opening-date").first().text();

                Movie item = new Movie(title, posterUrl, link,showdate,"Prime");
                movieList.add(item);
            }

        } catch (Exception e) {
            e.printStackTrace();
            callbackComingSoon.onError();
        }
        return movieList;
    }

    @Override
    protected void onPostExecute(List<Movie> movieList) {
        callbackComingSoon.onPostExecute(movieList);
    }
}

