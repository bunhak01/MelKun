package com.example.bunhak.melkun.Fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bunhak.melkun.Adapter.MyRecyclerViewAdapterMovieShowTime;
import com.example.bunhak.melkun.Class.Movie;
import com.example.bunhak.melkun.Interface.CallbackShowTime;
import com.example.bunhak.melkun.Parser.LegendParser.LegendShowTimeParser;
import com.example.bunhak.melkun.Parser.MajorParser.MajorShowTimeParser;
import com.example.bunhak.melkun.Parser.PrimeParser.PrimeShowTimeParser;
import com.example.bunhak.melkun.R;

import java.util.ArrayList;
import java.util.List;

public class ShowTimeMovieFragment extends Fragment implements CallbackShowTime {
    public String title;
    private RecyclerView recyclerView;
    private List<Movie> movieList;
    private ProgressDialog dialog;
    private MyRecyclerViewAdapterMovieShowTime adapter;

    public ShowTimeMovieFragment() {

    }

    public static ShowTimeMovieFragment newInstance(String title) {
        ShowTimeMovieFragment fragment = new ShowTimeMovieFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.title=title;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        movieList=new ArrayList<>();
        adapter = new MyRecyclerViewAdapterMovieShowTime(getContext(),movieList);
        dialog = new ProgressDialog(getContext());
        dialog.setCancelable(false);
        dialog.setMessage("Loading...");
        switch (title){
            case "Legend":
                new LegendShowTimeParser(this).execute();
                break;
            case "Major":
                new MajorShowTimeParser(this).execute();
                break;
            case "Prime":
                new PrimeShowTimeParser(this).execute();
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_show_time_movie, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        recyclerView= (RecyclerView) view.findViewById(R.id.rvShowtime);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }



    @Override
    public void onPreExecute() {
        dialog.show();
    }

    @Override
    public void onPostExecute(List<Movie> movieList) {
        this.movieList.addAll(movieList);
        adapter.clear();
        adapter.addMovie(movieList);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        dialog.dismiss();
    }

    @Override
    public void onError() {
        dialog.dismiss();
    }
}
