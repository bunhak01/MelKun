package com.example.bunhak.melkun.Interface;

import com.example.bunhak.melkun.Class.Movie;

import java.util.List;

/**
 * Created by bunha on 8/13/2017.
 */

public interface CallbackComingSoon {
    void onPreExecute();
    void onPostExecute(List<Movie> movieList);
    void onError();
}
