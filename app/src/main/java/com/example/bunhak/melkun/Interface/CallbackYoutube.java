package com.example.bunhak.melkun.Interface;

import com.example.bunhak.melkun.Class.Movie;

import java.util.List;

/**
 * Created by bunhak on 8/31/2017.
 */

public interface CallbackYoutube {
    void onPreExecute();
    void onPostExecute(String id);
    void onError();

}
