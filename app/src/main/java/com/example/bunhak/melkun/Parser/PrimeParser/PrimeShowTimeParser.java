package com.example.bunhak.melkun.Parser.PrimeParser;

import android.os.AsyncTask;
import android.util.Log;

import com.example.bunhak.melkun.Class.Movie;
import com.example.bunhak.melkun.Interface.CallbackShowTime;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bunha on 8/12/2017.
 */

public class PrimeShowTimeParser extends AsyncTask<Void, Void, List<Movie>> {

    private List<Movie> movieList;
    private CallbackShowTime callbackShowTime;

    public PrimeShowTimeParser(CallbackShowTime callbackShowTime) {
        movieList = new ArrayList<>();
        this.callbackShowTime = callbackShowTime;
    }

    @Override
    protected void onPreExecute() {
        callbackShowTime.onPreExecute();
    }
    @Override
    protected List<Movie> doInBackground(Void... voids) {
        String baseUrl = "http://202.62.37.232";
        String url = baseUrl + "/Browsing/Movies/NowShowing";

        Document document = null;
        try {
            document = Jsoup.connect(url).get();
            Elements movies = document.select("article#movies-list div.list-item.movie");
            Log.e("ooooo", String.valueOf(movies.size()) );
            for (Element movie : movies) {
                String posterUrl = "http:"+movie.select("div.image-outer img").first().attr("src");
                String title = movie.select("div.item-details h3.item-title").first().text();
                String link = "http:"+ movie.select("div.item-details div.title-wrapper a").first().attr("href");
                Movie item = new Movie(title, posterUrl, link,"Prime");
                movieList.add(item);
            }
        } catch (Exception e) {
            e.printStackTrace();
            callbackShowTime.onError();
        }
        return movieList;
    }

    @Override
    protected void onPostExecute(List<Movie> movieList) {
        callbackShowTime.onPostExecute(movieList);
    }
}

