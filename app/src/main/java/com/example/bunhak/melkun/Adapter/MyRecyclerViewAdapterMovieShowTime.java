package com.example.bunhak.melkun.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bunhak.melkun.Class.Movie;
import com.example.bunhak.melkun.Interface.CallbackYoutube;
import com.example.bunhak.melkun.Parser.LegendParser.LegendTrailerParser;
import com.example.bunhak.melkun.Parser.MajorParser.MajorTrailerParser;
import com.example.bunhak.melkun.Parser.PrimeParser.PrimeTrailerParser;
import com.example.bunhak.melkun.R;
import com.example.bunhak.melkun.YouTubeActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by bunha on 8/12/2017.
 */

public class MyRecyclerViewAdapterMovieShowTime extends RecyclerView.Adapter<MyRecyclerViewAdapterMovieShowTime.MyViewHolder> {

    private List<Movie> movieList;
    private Context context;

    public MyRecyclerViewAdapterMovieShowTime(Context context, List<Movie> movieList) {
        this.movieList = movieList;
        this.context = context;
    }

    public void clear(){
        movieList.clear();
    }
    public void addMovie(List<Movie> movieList){
        this.movieList.addAll(movieList);
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_item_showtime, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Movie movie = movieList.get(position);
        holder.tvTitle.setText(movie.getTitle());
        Picasso.with(context)
                .load(movie.getPosterUrl())
                .placeholder(R.drawable.waiting)
                .error(R.mipmap.ic_launcher)
                .into(holder.ivThumbnail);
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements CallbackYoutube,View.OnClickListener {

        TextView tvTitle;
        ImageView ivThumbnail;
        private int i;
        private ProgressDialog dialog;


        public MyViewHolder(View itemView) {
            super(itemView);
            dialog = new ProgressDialog(context);
            dialog.setCancelable(false);
            dialog.setMessage("Loading...");
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            ivThumbnail = (ImageView) itemView.findViewById(R.id.ivThumbnail);
            itemView.findViewById(R.id.btnTrailer).setOnClickListener(this);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(movieList.get(pos).getLink()));
                    context.startActivity(intent);
                }
            });
        }

        @Override
        public void onClick(View v) {
            i = getAdapterPosition();
            String link = movieList.get(i).getLink();
            switch (movieList.get(i).getCinema()) {
                case "Legend":
                    new LegendTrailerParser(this, link).execute();
                    break;
                case "Major":
                    new MajorTrailerParser(this,link).execute();
                    break;
                case "Prime":
                    new PrimeTrailerParser(this,link).execute();
                    break;
            }
        }

        @Override
        public void onPreExecute() {
            dialog.show();

        }

        @Override
        public void onPostExecute(String id) {
            dialog.dismiss();
            if(id==""){
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("No movie trailer") .setTitle(movieList.get(i).getCinema());
                builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                }
            else {
                Intent i=new Intent(context,YouTubeActivity.class);
                i.putExtra("trailerid",id);
                context.startActivity(i);
            }
        }

        @Override
        public void onError() {
            dialog.dismiss();
        }
    }
}
