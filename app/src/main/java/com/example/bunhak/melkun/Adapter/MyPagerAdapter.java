package com.example.bunhak.melkun.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.bunhak.melkun.Class.Tab;
import com.example.bunhak.melkun.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bunha on 8/12/2017.
 */

public class MyPagerAdapter extends FragmentStatePagerAdapter {
    List<Tab> tabList;
    Context context;

    public MyPagerAdapter(FragmentManager fm,Context context) {
        super(fm);
        tabList=new ArrayList<>();
        this.context=context;
    }

    public void clear(){
        tabList.clear();
    }

    @Override
    public Fragment getItem(int position) {
        return tabList.get(position).getFragment();
    }

    @Override
    public int getCount() {
        return tabList.size();
    }

    public void addTab(Tab tab) {
        this.tabList.add(tab);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabList.get(position).getTitle();
    }


    @Override
    public int getItemPosition(Object object){
        return POSITION_NONE;
    }

}
