package com.example.bunhak.melkun.Class;

/**
 * Created by bunha on 8/12/2017.
 */

public class Movie {
    private String title;
    private String cinema;

    public String getCinema() {
        return cinema;
    }

    public void setCinema(String cinema) {
        this.cinema = cinema;
    }

    private String posterUrl;
    private String link;
    private String ShowDate;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Movie(String title, String posterUrl, String link, String showDate,String cinema) {
        this.title = title;
        this.posterUrl = posterUrl;
        this.link = link;
        ShowDate = showDate;
        this.cinema=cinema;
    }

    public Movie(String title, String posterUrl, String link,String cinema) {
        this.title = title;
        this.posterUrl = posterUrl;
        this.link = link;
        this.cinema=cinema;
    }

    public String getShowDate() {

        return ShowDate;
    }

    public void setShowDate(String showDate) {
        ShowDate = showDate;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return this.title;
    }
}
