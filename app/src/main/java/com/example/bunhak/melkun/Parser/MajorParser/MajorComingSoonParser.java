package com.example.bunhak.melkun.Parser.MajorParser;

import android.os.AsyncTask;

import com.example.bunhak.melkun.Class.Movie;
import com.example.bunhak.melkun.Interface.CallbackComingSoon;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bunha on 8/12/2017.
 */

public class MajorComingSoonParser extends AsyncTask<Void, Void, List<Movie>> {

    private List<Movie> movieList;
    private CallbackComingSoon callbackComingSoon;

    public MajorComingSoonParser(CallbackComingSoon callbackComingSoon) {
        movieList = new ArrayList<>();
        this.callbackComingSoon = callbackComingSoon;
    }

    @Override
    protected void onPreExecute() {
        callbackComingSoon.onPreExecute();
    }
    @Override
    protected List<Movie> doInBackground(Void... voids) {
        String baseUrl = "http://majorcineplex.com.kh";
        String url = baseUrl + "/cinema/movies";
        Document document = null;
        try {
            document = Jsoup.connect(url).get();
            Elements movies = document.select("div#i_containTab2 li.detailContent1 div.contentTabs.clear div.row-month-comingsoon.clear div.row-movie-poster");
            for (Element movie : movies) {
                String posterUrl =movie.select("p.imgPoster img").first().attr("src");
                String title = movie.select("p.imgPoster a").first().attr("title").toString();
                String link =  movie.select("p.imgPoster a").first().attr("href");
                String showdate=movie.select("div.info-content.content-left p.txtBrown20_2").first().text();
                Movie item = new Movie(title, posterUrl, link,showdate,"Major");
                movieList.add(item);
            }

        } catch (Exception e) {
            e.printStackTrace();
            callbackComingSoon.onError();
        }
        return movieList;
    }

    @Override
    protected void onPostExecute(List<Movie> movieList) {
        callbackComingSoon.onPostExecute(movieList);
    }
}

