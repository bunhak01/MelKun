package com.example.bunhak.melkun.Parser.LegendParser;

import android.os.AsyncTask;
import android.util.Log;

import com.example.bunhak.melkun.Interface.CallbackYoutube;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by bunhak on 8/31/2017.
 */

public class LegendTrailerParser extends AsyncTask<Void, Void, String> {
    private String trailer;
    private CallbackYoutube callbackYoutube;
    private String link;

    public LegendTrailerParser(CallbackYoutube callbackYoutube, String link) {
        this.callbackYoutube = callbackYoutube;
        this.link=link;
    }

    @Override
    protected void onPreExecute() {
        callbackYoutube.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... voids) {
        Document document = null;
        String[] a;
        try {
            document = Jsoup.connect(link).get();
            trailer = document.select("div.movies-detail div#trailer-wrapper a").attr("href");
            if(trailer!=""){
                trailer= trailer.replaceAll("//","");
                a=trailer.split("v=");
                trailer=a[1];
            }


        } catch (Exception e) {
            e.printStackTrace();
            callbackYoutube.onError();
        }
        return trailer;
    }

    @Override
    protected void onPostExecute(String trailer) {
        callbackYoutube.onPostExecute(trailer);
    }
}
