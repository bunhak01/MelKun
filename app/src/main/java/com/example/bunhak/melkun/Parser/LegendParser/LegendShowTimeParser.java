package com.example.bunhak.melkun.Parser.LegendParser;

import android.os.AsyncTask;
import android.util.Log;

import com.example.bunhak.melkun.Class.Movie;
import com.example.bunhak.melkun.Interface.CallbackShowTime;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bunha on 8/12/2017.
 */

public class LegendShowTimeParser extends AsyncTask<Void, Void, List<Movie>> {

    private List<Movie> movieList;
    private CallbackShowTime callbackShowTime;

    public LegendShowTimeParser(CallbackShowTime callbackShowTime) {
        movieList = new ArrayList<>();
        this.callbackShowTime = callbackShowTime;
    }

    @Override
    protected void onPreExecute() {
        callbackShowTime.onPreExecute();
    }

    @Override
    protected List<Movie> doInBackground(Void... voids) {
        String baseUrl = "https://www.legend.com.kh";
        String url = baseUrl + "/Payments/ShowTime.aspx";
        Document document = null;
        try {
            document = Jsoup.connect(url).get();
            Elements movies = document.select("div.st_container.ses-first");

            for (Element movie : movies) {
                String posterUrl =movie.select("div.sp_sessions_img img").first().attr("src");
                String title = movie.select("div.sp_sessions_title a").first().text();
                String link =  movie.select("div.sp_sessions_img a").first().attr("href");
                Movie item = new Movie(title, posterUrl, link,"Legend");
                movieList.add(item);
            }
        } catch (Exception e) {
            e.printStackTrace();
            callbackShowTime.onError();
        }
        return movieList;
    }

    @Override
    protected void onPostExecute(List<Movie> movieList) {
        callbackShowTime.onPostExecute(movieList);
    }
}

