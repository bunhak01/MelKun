package com.example.bunhak.melkun.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bunhak.melkun.Adapter.MyPagerAdapter;
import com.example.bunhak.melkun.Class.Tab;
import com.example.bunhak.melkun.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TransactionFragment extends Fragment {

    TabLayout tabLayout;
    ViewPager viewPager;
    MyPagerAdapter adapter;
    public String item;

    public TransactionFragment() {

    }

    public static TransactionFragment newInstance(String i) {
        TransactionFragment fragment = new TransactionFragment();
        fragment.item=i;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_tansaction, container, false);
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        initView( view);
        adapter = new MyPagerAdapter(getFragmentManager(),getContext());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        adapter.clear();
        adapter.addTab(new Tab("Show Time", ShowTimeMovieFragment.newInstance(item)));
        adapter.addTab(new Tab("Coming Soon", ComingSoonFragment.newInstance(item)));
        Log.e("ooooo", adapter.getCount()+"");
        Log.e("ooooo", item+"");
        adapter.notifyDataSetChanged();
    }

    private void initView(View v) {

        tabLayout = (TabLayout) v.findViewById(R.id.tbShowMovie);
        viewPager = (ViewPager) v.findViewById(R.id.vpShowMovie);
    }
}
