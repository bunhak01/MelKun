package com.example.bunhak.melkun;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.example.bunhak.melkun.Fragment.TransactionFragment;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    Toolbar toolbar;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    ActionBarDrawerToggle actionBarDrawerToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        toolbar.setTitle("Mel Kun");
        setSupportActionBar(toolbar);
        setupDrawer();
        MenuItem defaultSelectedItem = navigationView.getMenu().getItem(0);
        defaultSelectedItem.setChecked(true);
        displaySelectedItem(defaultSelectedItem);

    }

    private void setupDrawer() {
        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.open,
                R.string.close
        );

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolBar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        navigationView = (NavigationView) findViewById(R.id.navigationView);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        displaySelectedItem(item);
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void displaySelectedItem(@NonNull MenuItem item) {
        FragmentManager fm1 = getSupportFragmentManager();
        FragmentTransaction ts1 = fm1.beginTransaction();
        TransactionFragment t= TransactionFragment.newInstance(item.getTitle().toString());
        Log.e("ooooo", "transac");
        ts1.replace(R.id.fragment_container, t);
        ts1.addToBackStack(null);
        ts1.commit();
    }


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            finish();
        }
    }
}
