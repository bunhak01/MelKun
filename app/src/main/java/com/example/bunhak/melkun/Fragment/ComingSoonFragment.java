package com.example.bunhak.melkun.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bunhak.melkun.Adapter.MyRecyclerViewAdapterMovieComingSoon;
import com.example.bunhak.melkun.Class.Movie;
import com.example.bunhak.melkun.Interface.CallbackComingSoon;
import com.example.bunhak.melkun.Parser.LegendParser.LegendComingSoonParser;
import com.example.bunhak.melkun.Parser.MajorParser.MajorComingSoonParser;
import com.example.bunhak.melkun.Parser.PrimeParser.PrimeComingSoonParser;
import com.example.bunhak.melkun.R;

import java.util.ArrayList;
import java.util.List;


public class ComingSoonFragment extends Fragment implements CallbackComingSoon {
    public String title;
    private RecyclerView recyclerView;
    private List<Movie> movieList;
    private ProgressDialog dialog;
    private MyRecyclerViewAdapterMovieComingSoon adapter;

    public ComingSoonFragment() {

    }

    public static ComingSoonFragment newInstance(String title) {
        ComingSoonFragment fragment = new ComingSoonFragment();
        Bundle args = new Bundle();
        fragment.title=title;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        movieList=new ArrayList<>();
        adapter = new MyRecyclerViewAdapterMovieComingSoon(getContext(),movieList);
        dialog = new ProgressDialog(getContext());
        dialog.setCancelable(false);
        dialog.setMessage("Loading...");
        switch (title){
            case "Legend":
                new LegendComingSoonParser(this).execute();
                break;
            case "Major":
                new MajorComingSoonParser(this).execute();
                break;
            case "Prime":
                new PrimeComingSoonParser(this).execute();
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_coming_soon, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        recyclerView= (RecyclerView) view.findViewById(R.id.rvComingSoon);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        movieList=new ArrayList<>();
        adapter = new MyRecyclerViewAdapterMovieComingSoon(getContext(),movieList);
    }


    @Override
    public void onPreExecute() {
        dialog.show();
    }

    @Override
    public void onPostExecute(List<Movie> movieList) {
        this.movieList.addAll(movieList);
        adapter.clear();
        adapter.addMovie(movieList);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        dialog.dismiss();
    }

    @Override
    public void onError() {
        dialog.dismiss();
    }
}
